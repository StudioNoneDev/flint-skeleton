<?php
use FlintExample\Controller\ExampleController;

return [
    'example' => function() {
        return new ExampleController();
    }
];
